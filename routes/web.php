<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('films');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/films','FilmsController@films_index')->name('films');

Route::get('/films/create','FilmsController@films_create')->name('films.create');

Route::post('/films/create','FilmsController@films_save');

Route::get('/films/{slug}','FilmsController@film_view')->name('film.details');

Route::post('/films/add_comment','FilmsController@add_comment')->name('film.add_comment');

Route::get('/film_photo/{filename}','FilmsController@file_view_photo')->name('film.photo');
