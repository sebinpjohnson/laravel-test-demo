@if ($paginator->hasPages())
    <ul class="pagination mx-auto" role="navigation">


        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active" aria-current="page"><span class="page-link current">{{ $page }}</span></li>
                    @else
                        <li class="page-item"><span class="page-link non-active" data-href="{{ $url }}">{{ $page }}</span></li>
                    @endif
                @endforeach
            @endif
        @endforeach
    </ul>
@endif
