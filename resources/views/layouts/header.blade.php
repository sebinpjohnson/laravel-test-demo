<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{route('films')}}">Laravel Test</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active {{ Request::is('films') ? 'active' : '' }}">
                <a class="nav-link"href="{{route('films')}}" >Films  </a>
            </li>
            @auth
            <li class="nav-item {{ Request::is('films/create') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('films.create')}}">Add Film </a>
            </li>
            @endauth
        </ul>
        <ul class="navbar-nav ml-auto">
            @guest
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="modal" data-target="#registerModal">Register</a>
            </li>
            @else
            <li class="nav-item">
            <a class="nav-link" href="#">
                    Hi, {{Auth::user()->name}}
            </a>
                </li>
            <li class="nav-item">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                     Logout
                 </a>

                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                 </form>
                </li>
            @endguest
        </ul>
    </div>
</nav>
