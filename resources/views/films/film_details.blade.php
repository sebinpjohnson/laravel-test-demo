@extends('layouts.laravel_test')

@section('content')

<div class="container">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{$film->name}}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{{$film->genre}}</h6>
            <div class="row">
                <div class="col-md-2">
                    <img src="{{ route('film.photo',$film->photo) }}" alt="{{$film->name}} Image" class="img-fluid">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <strong>Rating : </strong>
                        <span class="ratings pl-1">
                            @switch($film->rating)
                            @case($film->rating == 1)
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            @break
                            @case($film->rating == 2)
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            @break
                            @case($film->rating == 3)
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            @break
                            @case($film->rating == 4)
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star " aria-hidden="true"></i>
                            @break
                            @case($film->rating == 5)
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            <i class="fa fa-star rating" aria-hidden="true"></i>
                            @break
                            @default

                            @endswitch
                        </span>
                    </div>

                    <div class="row">
                        <strong>Country : </strong>
                        <span class="pl-1"> {{$film->country}}</span>
                    </div>
                    <div class="row">
                        <strong>Film Description : </strong>
                        <p>
                            {{$film->description}}
                        </p>
                    </div>
                    <div class="row">
                        <strong>Release Date : </strong>
                        <span class="release_date pl-1">
                            {{ date('d-m-Y',strtotime($film->release_date)) }}
                        </span>
                    </div>

                    <div class="row">
                        <strong>Ticket Price : </strong>
                        <span class="ticket_price pl-1 rating">
                            <b>USD {{$film->ticket_price}}</b>
                        </span>
                    </div>

                </div>
            </div>

        </div>

        <div class="container mb-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Comments : </h5>
                    <hr>
                    <div class="comments-section">
                        @foreach ($film->comments as $comment)

                        <div class="card px-3 py-3 mt-2">
                            <h6 class="card-subtitle mb-2 text-muted">{{$comment->user->name}}</h6>
                            <p class="card-text">{{$comment->comment}}</p>
                        </div>
                        @endforeach
                    </div>
                    <div class="add-comments mt-3">
                        @guest
                        <div class="card px-3 py-3 text-center">
                            <b class="text-danger">Login / Register to comment</b>
                        </div>
                        @else
                        <div class="card px-3 py-3">
                            <form action="#" id="comment-form" data-url="{{route('film.add_comment')}}">
                                @csrf
                                <input type="hidden" name="film_id" value="{{$film->id}}">
                                <h6 class="card-subtitle mb-2 ">User: <span class="text-muted"> {{ Auth::user()->name
                                        }} </span></h6>
                                <textarea name="comment" id="comment" class="form-control" cols="30" rows="4"
                                    placeholder="Comment"></textarea>
                                <div class="form-group mt-2 text-right">
                                    <button type="button" class="btn btn-success add_comment">Add Comment</button>
                                </div>
                            </form>
                        </div>
                        @endguest
                    </div>

                </div>
            </div>
        </div>

    </div>


</div>

@endsection

@section('footer')
<script src="{{ asset('js/film_detail.js') }}"></script>
@endsection
