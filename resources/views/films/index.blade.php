@extends('layouts.laravel_test')

@section('content')
<div class="container mb-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @if($films->count() == 0)
                                <h4>No Films Found</h4>
                                @else
                                <h4>Films :</h4>
                                @endif
                            </div>
                        </div>
                        <div id="films-section">
                            @include('films.partial.film')
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
@endsection
