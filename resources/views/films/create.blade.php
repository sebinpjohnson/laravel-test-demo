@extends('layouts.laravel_test')

@section('content')
<div class="container">
    <div class="card mb-4">
        <form action="{{ route('films.create') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="film_name">Film Name</label>
                            <input type="text" name="film_name" id="film_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="film_description">Film Description</label>
                            <textarea name="film_descrption" id="film_descrption" cols="30" class="form-control" rows="10"
                                required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="release_date">Release Date</label>
                            <input type="date" name="release_date" id="release_date" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="rating">Rating</label>
                            <select name="rating" id="rating" class="form-control" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ticket_price">Ticket Price</label>
                            <input type="number" name="ticket_price" id="ticket_price" class="form-control" min="0" required>
                        </div>
                        <div class="form-group">
                            <label for="country">Country</label>
                            <select name="country" id="country" style="width:100%" class="form-control" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="genre">Genre</label>
                            <select name="genre[]" id="genre" style="width:100%" class="form-control" multiple required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="photo">Photo</label>
                            <input type="file" name="photo" id="photo" class="form-control" required>
                        </div>
                        <div class="form-group text-right">
                            <a href="{{ route('films') }}" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer')
<script src="{{asset('js/films.js')}}"></script>
@endsection
