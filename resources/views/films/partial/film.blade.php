<div class="row ">

    @foreach($films as $film)
    <div class="offset-md-2 col-md-8 d-flex mt-3">
        <div class="card">
            <div class="film-loader text-center pt-5">
                <i class="fa fa-spin fa-4x fa-spinner"></i>
            </div>
            <img class="card-img-top" src="{{ route('film.photo',$film->photo) }}" alt="Card image cap">
            <div class="card-body flex-fill">
                <h5 class="card-title"> <a href="{{ route('film.details',$film->slug)}}">{{$film->name}}</a>
                </h5>
                <p class="ratings">
                    <span class="ratings ">
                        @switch($film->rating)
                        @case($film->rating == 1)
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        @break
                        @case($film->rating == 2)
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        @break
                        @case($film->rating == 3)
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        @break
                        @case($film->rating == 4)
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star " aria-hidden="true"></i>
                        @break
                        @case($film->rating == 5)
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        <i class="fa fa-star rating" aria-hidden="true"></i>
                        @break
                        @default

                        @endswitch
                    </span>
                </p>
                <p class="card-text film-description ">{{$film->description}}</p>
                <p class="genre"><strong>Genre :</strong> {{$film->genre}}</p>
                <p class="ticket_price"><strong>Ticket Price :</strong> <span class="rating">
                        <b>USD {{$film->ticket_price}}</b> </span></p>
            </div>
            <div class="card-footer">
                <small class="text-muted">Release Date : {{
                    date('d-m-Y',strtotime($film->release_date)) }}</small>
            </div>

        </div>
    </div>
    @endforeach
</div>

<div class="row mt-2">
    <div class="offset-md-2 col-md-8 d-flex mt-3">
        {!! $films->render() !!}
    </div>
</div>
