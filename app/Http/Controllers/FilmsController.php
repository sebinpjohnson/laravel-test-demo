<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Request;
use App\Models\Comments;
use App\Models\Films;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class FilmsController extends Controller
{
    public function films_index(Request $request)
    {
        if ($request->ajax()) {
            return view('films.partial.film', [
                'films' => Films::paginate(1),
            ]);
        }

        return view('films.index', [
            'films' => Films::paginate(1),
            'title' => 'Films Index',
        ]);
    }

    public function films_create()
    {
        if (\Auth::guest()) {
            abort(403);
        }
        return view('films.create', ['title' => 'Add Film']);
    }

    public static function slug_gen($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text) . '-' . uniqid();

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function file_view_photo($filename)
    {
        if (!\Storage::exists('film_photo/' . $filename)) {
            abort(404);
        }
        return \Storage::download('film_photo/' . $filename);
    }

    public function films_save(Request $request)
    {
        $film = new Films;
        $film->name = $request->film_name;
        $film->description = $request->film_descrption;
        $film->release_date = Carbon::createFromFormat('Y-m-d', $request->release_date)->format('Y-m-d H:i:s');
        $film->rating = $request->rating;
        $film->ticket_price = $request->ticket_price;
        $film->country = $request->country;
        $film->genre = implode(',', $request->genre);
        $film->slug = $this->slug_gen($film->name);
        $request->file('photo')->store('film_photo');
        $film->photo = $request->file('photo')->hashName();
        $film->user_id = \Auth::user()->id;
        $film->save();
        $films = Films::get();
        return redirect('films');
    }

    public function film_view($slug)
    {
        $film = Films::where('slug', $slug)
            ->with('comments.user')
            ->first();
        if ($film === null) {
            abort(404);
        }
        return view('films.film_details', [
            'film' => $film,
            'title' => $film->name . ' - Details',
        ]);
    }

    public function add_comment(Request $request)
    {
        $comment = new Comments;
        $comment->film_id = $request->film_id;
        $comment->user_id = \Auth::user()->id;
        $comment->comment = $request->comment;
        $comment->save();
        return response()->json([
            'success' => true,
            'result' => $comment,
            'user' => \Auth::user(),
        ]);
    }
}
