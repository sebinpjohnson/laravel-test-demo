<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    //
    public function user(){
        return $this->belongsTo('\App\Models\User','user_id');
    }

    public function films(){
        return $this->belongsTo('\App\Models\Films','film_id');
    }
}
