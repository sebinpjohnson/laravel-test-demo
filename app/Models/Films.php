<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Films extends Model
{
    //
    public function comments(){
        return $this->hasMany('\App\Models\Comments','film_id');
    }

    public function user(){
        return $this->belongsTo('\App\Models\User','user_id');
    }

}
