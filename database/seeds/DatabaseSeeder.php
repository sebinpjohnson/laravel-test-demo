<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 1)->create()->each(function ($user) {
            $films = factory(App\Models\Films::class,3)->create(['user_id' => $user->id])->each(function ($film){
                $comment = factory(App\Models\Comments::class,1)->create(['user_id' => $film->user_id,'film_id' => $film->id]);
                $film->comments()->saveMany($comment);
            });
            $user->films()->saveMany($films);
        });
    }
}
