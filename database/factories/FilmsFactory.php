<?php
use App\Models\Comments;
use App\Models\User;
use App\Models\Films;

use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});


$factory->define(Films::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'country' => $faker->country,
        'genre' => implode(',',$faker->words($nb = 3)),
        'photo' => $faker->image(storage_path('app').'/film_photo',400,300, null, false),
        'release_date' => $faker->date($format = 'Y-m-d'),
        'description' => $faker->sentence($nbWords = 20, $variableNbWords = true) ,
        'rating' => $faker->randomElement([1,2,3,4,5]),
        'slug' => uniqid(),
        'ticket_price' => $faker->numberBetween(10,20)
    ];
});

$factory->define(Comments::class, function (Faker $faker) {
    return [
        'comment' => $faker->sentence($nbWords = 8, $variableNbWords = true)
    ];
});


