$(document).on('click', '.add_comment', function () {
    var commentForm = $('#comment-form'),
        url = commentForm.data('url'),
        commentCards = $('.comments-section');
    var commentLoading = '<div class="card loading-comment px-3 py-3 mt-2">' +
        '<h6 class="card-subtitle mb-2 text-muted"></h6>' +
        '<p class="card-text text-center"><i class="fa fa-spinner fa-spin fa-3x"></i></p></div>';
    if ($('[name="comment"]').val() == '') {
        alertify.error('Please enter a comment');
    } else {
        commentCards.append(commentLoading);
        $.ajax({
            url: url,
            data: commentForm.serialize(),
            method: 'POST',
            success: function (res) {
                $(document).find('.loading-comment').remove();
                commentCards.append('<div class="card px-3 py-3 mt-2">' +
                    '<h6 class="card-subtitle mb-2 text-muted">' + res.user.name + '</h6>' +
                    '<p class="card-text ">' + res.result.comment + '</p></div>');
                    $('[name="comment"]').val('')
            },
            error: function (err) {
                $(document).find('.loading-comment').remove();
                alertify.error('An error occured kindly check if all fields are entered');
            }
        })
    }
});
