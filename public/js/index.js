

$(document).ready(function () { // this function is used to watch click events on pagination
    $(document).on('click', '.pagination span.non-active', function (event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        $('.film-loader').show();
        var myurl = $(this).attr('data-href');
        var page = $(this).attr('data-href').split('page=')[1];
        getData(page);
    });

    $(document).on('click', '.pagination span.currentz', function (event) {
        $('li').removeClass('active');
        $('.film-loader').show();
        $(this).parent('li').addClass('active');
        event.preventDefault();
        getData(1);
    });

});

/**
 * This function is used to get next film details
 * @param {*} page
 */
function getData(page) { // used to fetch film details from api
    $.ajax({
            url: '?page=' + page,
            type: "get",
            datatype: "html",
        })
        .done(function (data) {
            $("#films-section").empty().html(data);
            $('.film-loader').hide();
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            $('.film-loader').hide();
            alertify.error('No response from server');
        });
}
