$(document).ready(function () {
    var errorModel = $('#errorModal');
    if (errorModel.data('error')) {
        errorModel.modal('show');
        // errorModel.find('.errors').show();
    }
    $(document).on('click', '.hide-error', function () {
        errorModel.modal('hide');
    })
});
